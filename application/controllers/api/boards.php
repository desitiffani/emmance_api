<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Boards extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('board');
    }

    public function board_get()
    {
        if($this->get('id'))
        {
            $data = $this->board->get(array('id'=>$this->get('id')))->num_rows();
            if($data){
            	$this->response($data, 200);
            }else{
                $this->response(array('error' => 'Board could not be found'), 404);
            }
        }else{
            $data = $this->board->get()->result_array();
            $this->response($data, 200);
        }
    }

    public function board_post(){
        $data = json_decode(file_get_contents('php://input'),true);
        if(!$data){
            $this->response(NULL, 400);
        }else{
            $exist = $this->board->get(array('id'=>$data['id']))->num_rows();
            if($exist == 0){
                $data_board = array(
                    'id' => $data['id'],
                    'name' => $data['name'],
                    'url' => $data['shortUrl']
                );
                $this->board->add($data_board);
            }
            $this->response("Board saved.", 200);
        }
    }
}
