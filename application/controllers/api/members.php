<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Members extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('member', 'card'));
    }

    public function member_get()
    {
        if($this->get('id'))
        {
            $data['member'] = $this->member->get(array('id'=>$this->get('id')))->row_array();
            $data['totalCard'] = $this->card->get_card_member(array('member.id'=>$this->get('id')))->num_rows();
            $data['current_sprint'] = $this->member->get_detail(array('member.id'=>$this->get('id')), array('list.name' => 'current'))->result_array();
            $data['next_sprint'] = $this->member->get_detail(array('member.id'=>$this->get('id')), array('list.name' => 'next'))->result_array(); 
            $data['in_progress'] = $this->member->get_detail(array('member.id'=>$this->get('id')), array('list.name' => 'progress'))->result_array();
            $data['in_review'] = $this->member->get_detail(array('member.id'=>$this->get('id')), array('list.name' => 'review'))->result_array();
            $data['done'] = $this->member->get_detail(array('member.id'=>$this->get('id')), array('list.name' => 'done'))->num_rows();
            $data['done_detail'] = $this->member->get_detail(array('member.id'=>$this->get('id')), array('list.name' => 'done'))->result_array();
            $data['task_this_week'] = $this->member->get_detail(array('member.id'=>$this->get('id')), NULL, true)->num_rows();
            $data['task_this_week_detail'] = $this->member->get_detail(array('member.id'=>$this->get('id')), NULL, true)->result_array();
            $data['done_this_week'] = $this->member->get_detail(array('member.id'=>$this->get('id')), array('list.name' => 'Done'), true)->num_rows();
            $data['done_this_week_detail'] = $this->member->get_detail(array('member.id'=>$this->get('id')), array('list.name' => 'Done'), true)->result_array();
            $data['performance'] = ($data['done_this_week']/$data['task_this_week'])*100;
            if($data['performance'] >= 50){
                $data['result'] = 'Productive';
                $data['result_class'] = 'green';
            }else if($data['performance'] < 50){
                $data['result'] = 'Not Productive';
                $data['result_class'] = 'red';
            }
            if($data){
            	$this->response($data, 200);
            }else{
                $this->response(array('error' => 'Member could not be found'), 404);
            }
        }else{
            $data = array();
            $i = 0;
            $members = $this->member->get()->result_array();
            foreach($members as $member){
               $data[$i]['member'] = $member;
               $data[$i]['totalCard'] = $this->card->get_card_member(array('member.id'=>$member['id']))->num_rows();
               $data[$i]['done'] = $this->member->get_detail(array('member.id'=>$member['id']), array('list.name' => 'Done'))->num_rows();
               $data[$i]['task_this_week'] = $this->member->get_detail(array('member.id'=>$member['id']), NULL, true)->num_rows();
               $data[$i]['done_this_week'] = $this->member->get_detail(array('member.id'=>$member['id']), array('list.name' => 'Done'), true)->num_rows();
               $data[$i]['performance'] = ($data[$i]['done_this_week']/$data[$i]['task_this_week'])*100;
               if($data[$i]['performance'] >= 50){
                    $data[$i]['result'] = 'Productive';
                    $data[$i]['result_class'] = 'green';
               }else if($data[$i]['performance'] < 50){
                    $data[$i]['result'] = 'Not Productive';
                    $data[$i]['result_class'] = 'red';
               }
               $i++;
            }
            $this->response($data, 200);
        }
    }

    public function member_post(){
        $data = json_decode(file_get_contents('php://input'),true);
        if(!$data){
            $this->response(NULL, 400);
        }else{
            $exist = $this->member->get(array('id'=>$data['id']))->num_rows();
            if($exist == 0){
                $this->member->add($data);
            }
            $this->response("Member saved.", 200);
        }
    }
}
