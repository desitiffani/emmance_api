<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Cards extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('card', 'member'));
    }

    public function card_get()
    {
        if($this->get('id'))
        {
            $data = $this->card->get(array('id'=>$this->get('id')))->num_rows();
            if($data){
            	$this->response($data, 200);
            }else{
                $this->response(array('error' => 'Card could not be found'), 404);
            }
        }else{
            $data = $this->card->get()->result_array();
            $this->response($data, 200);
        }
    }

    public function card_post(){
        $data_post = json_decode(file_get_contents('php://input'),true);
        if($data_post){
            $card_data = $data_post['card'];
            $card_list = $data_post['card_list'];

            $data = array(
                'id'        => $card_data['id'],
                'name'      => $card_data['name'],
                'created'   => $card_data['dateLastActivity']
            );

            $exist = $this->card->get(array('id'=>$data['id']))->num_rows();
            if($exist == 0){
                $this->card->add($data);
            }

            //save card members
            foreach ($card_data['idMembers'] as $key => $idMember) {
                $exist = $this->card->get_card_member(array('card_member.card_id'=>$card_data['id'], 'card_member.member_id' => $idMember))->num_rows();
                if($exist == 0){
                    $exist_member = $this->member->get(array('id' => $idMember))->num_rows();
                    if($exist_member){
                        $this->card->add_card_member(array('card_id'=>$card_data['id'], 'member_id' => $idMember));
                    }
                }
            }

            //save card lists
            foreach ($card_list as $key => $list) {
                $exist = $this->card->get_card_list(array('card_list.list_id'=>$list['data']['listAfter']['id'], 'card_list.card_id'=>$list['data']['card']['id']))->num_rows();
                if($exist == 0){
                    $data_list = array('list_id' => $list['data']['listAfter']['id'],
                                       'card_id' => $list['data']['card']['id'],
                                       'updated' => $list['date']);
                    $this->card->add_card_list($data_list);
                }
            }

            /*$exist = $this->card->get_card_list(array('card_list.list_id'=>$card_list['idList'], 'card_list.card_id'=>$card_list['id']))->num_rows();
            if($exist == 0){
                $data_list = array('list_id' => $card_list['idList'],
                                   'card_id' => $card_list['id'],
                                   'updated' => $card_list['dateLastActivity']);
                $this->card->add_card_list($data_list);
            }*/


            $this->response("Card saved.", 200);

        }else{
           $this->response(NULL, 400);
        }
    }
}
