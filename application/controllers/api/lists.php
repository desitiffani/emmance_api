<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Lists extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('list_model');
    }

    public function list_get()
    {
        if($this->get('id'))
        {
            $data = $this->list_model->get(array('id'=>$this->get('id')))->num_rows();
            if($data){
            	$this->response($data, 200);
            }else{
                $this->response(array('error' => 'List could not be found'), 404);
            }
        }else{
            $data = $this->list_model->get()->result_array();
            $this->response($data, 200);
        }
    }

    public function list_post(){
        $data = json_decode(file_get_contents('php://input'),true);
        if(!$data){
            $this->response(NULL, 400);
        }else{
            $exist = $this->list_model->get(array('id'=>$data['id']))->num_rows();
            if($exist == 0){
                $data_list = array(
                    'id' => $data['id'],
                    'name' => $data['name'],
                    'board_id' => $data['idBoard']
                );
                $this->list_model->add($data_list);
            }
            $this->response("List saved.", 200);
        }
    }
}
