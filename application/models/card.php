<?php
class Card extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('card');
	}

	function add($data){
		$this->db->insert('card',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('id',$id);
		return $this->db->update('card',$data);
	}
	
	function delete($id){
		$this->db->where('id',$id);
		return $this->db->delete('card');
	}

	/*CARD MEMBER*/
	function get_card_member($where = NULL){
		$this->db->select('member.*, card.*');
		$this->db->from('card_member');
		$this->db->join('member', 'card_member.member_id = member.id');
		$this->db->join('card', 'card_member.card_id = card.id');
		if($where){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function add_card_member($data){
		$this->db->insert('card_member',$data);
		return $this->db->insert_id();
	}
	
	function delete_card_member($where){
		$this->db->where($where);
		return $this->db->delete('card_member');
	}

	/*CARD LIST*/
	function get_card_list($where = NULL){
		$this->db->select('list.*, card.*');
		$this->db->from('card_list');
		$this->db->join('card', 'card_list.card_id = card.id');
		$this->db->join('list', 'card_list.list_id = list.id');
		if($where){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function add_card_list($data){
		$this->db->insert('card_list',$data);
		return $this->db->insert_id();
	}
}