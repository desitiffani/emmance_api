<?php

class List_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('list');
	}

	function add($data){
		$this->db->insert('list',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('id',$id);
		return $this->db->update('list',$data);
	}
	
	function delete($id){
		$this->db->where('id',$id);
		return $this->db->delete('list');
	}
}