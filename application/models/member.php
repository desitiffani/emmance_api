<?php
class Member extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('member');
	}

	function get_member_card_view($where = NULL){
		$this->db->select('*');
		$this->db->from('cek_card_member');
		$this->db->where('member_id IS NULL AND (created BETWEEN "2015-10-09 09:04:36" AND "2016-01-04 07:31:48")');
		//$this->db->limit(100);
		$this->db->order_by('created', 'ASC');
		return $this->db->get();
	}

	function add($data){
		$this->db->insert('member',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('id',$id);
		return $this->db->update('member',$data);
	}
	
	function delete($id){
		$this->db->where('id',$id);
		return $this->db->delete('member');
	}

	function get_member_card($where = NULL){
		$this->db->select('member.*, card.*');
		$this->db->from('member');
		$this->db->join('card_member', 'member.id = card_member.member_id');
		$this->db->join('card', 'card_member.card_id = card.id');
		if($where){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_max_date(){
		$this->db->select('max(card_list.updated) as max_updated');
		$this->db->from('card_list');
		$this->db->group_by('card_list.card_id');
		$max = $this->db->get()->result_array();
		$return_data = array();
		foreach ($max as $key => $value) {
			$return_data[] = $value['max_updated'];
		}
		return $return_data;
	}

	function get_detail($where = NULL, $like = NULL, $this_week = false){
		$max_updated = $this->get_max_date();
		$this->db->select('member.username,card.id as card_id,card.name as card_name, list.name as list_name, card_list.updated, board.name as board_name');
		$this->db->from('member');
		$this->db->join('card_member', 'member.id = card_member.member_id');
		$this->db->join('card', 'card_member.card_id = card.id');
		$this->db->join('card_list', 'card.id = card_list.card_id', 'right');
		$this->db->join('list', 'card_list.list_id = list.id');
		$this->db->join('board', 'list.board_id = board.id');
		if($where){
			$this->db->where($where);
		}
		if($like){
			$this->db->where_in('card_list.updated', $max_updated);
			$this->db->like($like);
		}
		if($this_week){
			$this->db->where('card_list.updated > DATE_SUB(NOW(), INTERVAL 1 WEEK)');
		}
		$this->db->group_by('card.id');
		$this->db->order_by('card_list.updated', 'DESC');
		return $this->db->get();
	}
}